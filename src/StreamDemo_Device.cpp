#ifdef LINUX
#include <stdlib.h>
#include <unistd.h> 
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h> 
#include <sys/types.h>
#include <sys/time.h> 
#include <signal.h> 
#include <netinet/in.h>
#include <netdb.h> 
#include <net/if.h>
#include <string.h>
#include <sched.h>
#include <stdarg.h>
#include <dirent.h>
#include <arpa/inet.h>  // inet_ntoa
#endif
#ifdef WIN32DLL
#include <windows.h>
#include <stdio.h>
#include <direct.h>
#endif

#include "STARX/STARX_API.h"
#include "AVSTREAM_IO_Proto.h"

#define AVSTREAM_TEST

#ifdef WIN32DLL
UINT32 MyGetTickCount() {return GetTickCount();}
#endif
#ifdef LINUX
CHAR bFlagGetTickCount = 0;
struct timeval gTime_Begin;
UINT32 MyGetTickCount() {
	if (!bFlagGetTickCount) {
		bFlagGetTickCount = 1;
		gettimeofday(&gTime_Begin, NULL);
		return 0;
	}
	struct timeval tv;
	gettimeofday(&tv, NULL);
	//printf("%d %d %d %d\n",tv.tv_sec , gTime_Begin.tv_sec, tv.tv_usec ,gTime_Begin.tv_usec);
	return (tv.tv_sec - gTime_Begin.tv_sec) * 1000
			+ (tv.tv_usec - gTime_Begin.tv_usec) / 1000;
}
#endif

void mSecSleep(UINT32 ms) {
#ifdef WIN32DLL
	Sleep(ms);
#endif //// WIN32DLL
#ifdef LINUX
	usleep(ms * 1000);
#endif //// LINUX
}

#ifdef AVSTREAM_TEST
#define MAX_SIZE_BUF	64*1024
#define CHANNEL_DATA	1
#define CHANNEL_IOCTRL	2
#define TIME_SPAN_IDLE	20 //in ms
#define MAX_USER		8

typedef struct {
	CHAR bUsed;
	CHAR bVideoRequested;
	CHAR bAudioRequested;
	CHAR bVideoGo;
	INT32 SessionHandle;
} st_User;
st_User gUser[MAX_USER];
INT32 gRunning = 1;

void myReleaseUser(st_User *pstUser) {
	if (pstUser == NULL)
		return;
	if (pstUser->SessionHandle >= 0)
		STARX_Close(pstUser->SessionHandle);
	memset(pstUser, 0, sizeof(st_User));
}

#if defined(WIN32DLL)
#define mSecSleep(ms)	Sleep(ms)
typedef DWORD STARX_threadid;
#elif defined(LINUX)
#define mSecSleep(ms)	usleep(ms * 1000)
typedef pthread_t STARX_threadid;
#endif

ULONG myGetTickCount() {
#if defined(WIN32DLL)
	return GetTickCount();

#elif defined(LINUX)
	ULONG currentTime;
	struct timeval current;
	gettimeofday(&current, NULL);
	currentTime = current.tv_sec * 1000 + current.tv_usec / 1000;
	return currentTime;
#endif
}

INT32 myGetDataSizeFrom(st_AVStreamIOHead *pStreamIOHead) {
	INT32 nDataSize = pStreamIOHead->nStreamIOHead;
	nDataSize &= 0x00FFFFFF;
	return nDataSize;
}

void myDoIOCtrl(INT32 iIndex, CHAR *pData) {
	st_AVIOCtrlHead stIOCtrlHead;
	memcpy(&stIOCtrlHead, pData, sizeof(st_AVIOCtrlHead));
	switch (stIOCtrlHead.nIOCtrlType) {
	case IOCTRL_TYPE_VIDEO_START:
		gUser[iIndex].bVideoRequested = 1;
		printf("myDoIOCtrl(..): %d, IOCTRL_TYPE_VIDEO_START\n", iIndex);
		break;

	case IOCTRL_TYPE_VIDEO_STOP:
		gUser[iIndex].bVideoRequested = 0;
		printf("myDoIOCtrl(..): %d, IOCTRL_TYPE_VIDEO_STOP\n", iIndex);
		break;

	case IOCTRL_TYPE_AUDIO_START:
		gUser[iIndex].bAudioRequested = 1;
		printf("myDoIOCtrl(..): %d, IOCTRL_TYPE_AUDIO_START\n", iIndex);
		break;

	case IOCTRL_TYPE_AUDIO_STOP:
		gUser[iIndex].bAudioRequested = 0;
		printf("myDoIOCtrl(..): %d, IOCTRL_TYPE_AUDIO_STOP\n", iIndex);
		break;
	default:
		;
	}
}

#ifdef WIN32DLL
DWORD WINAPI myThreadRecvIOCtrl(void* arg)
#endif
#ifdef LINUX
void *myThreadRecvIOCtrl(void *arg)
#endif
		{
	INT32 i = (INT32) arg, nRet = -1;
	CHAR buf[1024];
	INT32 nRecvSize = 4;
	st_AVStreamIOHead *pStreamIOHead = NULL;

	do {
		nRecvSize = sizeof(st_AVStreamIOHead);
		nRet = STARX_Read(gUser[i].SessionHandle, CHANNEL_IOCTRL, buf,
				&nRecvSize, 0xFFFFFFFF);
		if (!(nRet == ERROR_STARX_TIME_OUT || nRet == ERROR_STARX_SUCCESSFUL)) {
			myReleaseUser(&gUser[i]);
			printf("myThreadRecvIOCtrl: failed %d !!\n", nRet);
			break;
		}

		if (nRecvSize > 0) {
			pStreamIOHead = (st_AVStreamIOHead *) buf;
			nRecvSize = myGetDataSizeFrom(pStreamIOHead);
			nRet = STARX_Read(gUser[i].SessionHandle, CHANNEL_IOCTRL, buf,
					&nRecvSize, 0xFFFFFFFF);
			if (!(nRet == ERROR_STARX_TIME_OUT || nRet == ERROR_STARX_SUCCESSFUL)) {
				myReleaseUser(&gUser[i]);
				printf("myThreadRecvIOCtrl: failed %d !!\n", nRet);
				break;

			}
			if (nRecvSize > 0)
				myDoIOCtrl(i, buf);
		}
	} while (gRunning);

	printf("---%d, myThreadRecvIOCtrl exit!!\n", i);

#ifdef WIN32DLL
	return 0L;
#endif
#ifdef LINUX
	pthread_exit(0);
#endif
}

void AudioFrameReady(CHAR *Buf, INT32 nSize, st_AVFrameHead *pFreamHead) {
	INT32 UserCount = 0, nRet = 0, wsize = 0;

	for (INT32 i = 0; i < MAX_USER; i++) {
		if (gUser[i].bUsed)
			UserCount++;
	}
	pFreamHead->nOnlineNum = UserCount;

	for (INT32 i = 0; i < MAX_USER; i++) {
		if (!gUser[i].bUsed || !gUser[i].bAudioRequested)
			continue;

		nRet = STARX_Check_Buffer(gUser[i].SessionHandle, 1, (UINT32 *) &wsize,
		NULL);
		if (nRet == ERROR_STARX_SESSION_CLOSED_TIMEOUT) {
			myReleaseUser(&gUser[i]);
			printf("AudioFrameReady: %d, Session TimeOUT!!\n", i);

		} else if (nRet == ERROR_STARX_SESSION_CLOSED_REMOTE) {
			myReleaseUser(&gUser[i]);
			printf("AudioFrameReady: %d, Session Remote Close!!\n", i);

		} else if (nRet == ERROR_STARX_INVALID_SESSION_HANDLE) {
			myReleaseUser(&gUser[i]);
			printf("AudioFrameReady: %d, invalid session handle!!\n", i);
		}
		if (wsize > 65535)
			continue; //64*1024
		nRet = STARX_Write(gUser[i].SessionHandle, CHANNEL_DATA, Buf, nSize);
	}
}

void VideoFrameReady(CHAR *Buf, INT32 nSize, st_AVFrameHead *pFreamHead) {
	INT32 UserCount = 0, nRet = 0, wsize = 0;
	for (INT32 i = 0; i < MAX_USER; i++) {
		if (gUser[i].bUsed)
			UserCount++;
	}
	pFreamHead->nOnlineNum = UserCount;

	for (INT32 i = 0; i < MAX_USER; i++) {
		if (!gUser[i].bUsed || !gUser[i].bVideoRequested)
			continue;

		if (gUser[i].bVideoGo == 0)
			continue;

		nRet = STARX_Check_Buffer(gUser[i].SessionHandle, 1, (UINT32 *) &wsize,
		NULL);
		if (nRet == ERROR_STARX_SESSION_CLOSED_TIMEOUT) {
			myReleaseUser(&gUser[i]);
			printf("VideoFrameReady: %d, Session TimeOUT!!\n", i);

		} else if (nRet == ERROR_STARX_SESSION_CLOSED_REMOTE) {
			myReleaseUser(&gUser[i]);
			printf("VideoFrameReady: %d, Session Remote Close!!\n", i);

		} else if (nRet == ERROR_STARX_INVALID_SESSION_HANDLE) {
			myReleaseUser(&gUser[i]);
			printf("VideoFrameReady: %d, invalid session handle!!\n", i);
		}
		if (wsize > 65535) {
			gUser[i].bVideoGo = 0;
			continue; //64*1024
		}
		nRet = STARX_Write(gUser[i].SessionHandle, CHANNEL_DATA, Buf, nSize);
	}
}

int h264_GetOneFrameFromFile(FILE* fp, CHAR* pBufArg) {
	int nPos1 = 0, nPos2 = 0;
	bool bEOF = false;
	CHAR buf2[8] = { 0 };

	while (1) { //find 1st start code
		if (feof(fp)) {
			bEOF = true;
			break;
		}
		pBufArg[nPos1] = fgetc(fp);
		if (pBufArg[nPos1] == 0)
			nPos1++;
		else
			nPos1 = 0;
		if (nPos1 == 3) {
			if (feof(fp)) {
				bEOF = true;
				break;
			}
			pBufArg[nPos1] = fgetc(fp);
			if (pBufArg[nPos1] == 1) {
				nPos1++;
				break;
			} else
				nPos1 = 0;
		}
	}
	if (bEOF && nPos1 < 4)
		return -1;

	while (1) { //find 2nd start code
		if (feof(fp)) {
			bEOF = true;
			break;
		}
		buf2[nPos2] = fgetc(fp);
		pBufArg[nPos1++] = buf2[nPos2]; //important
		if (buf2[nPos2] == 0)
			nPos2++;
		else
			nPos2 = 0;
		if (nPos2 == 3) {
			if (feof(fp)) {
				bEOF = true;
				break;
			}
			buf2[nPos2] = fgetc(fp);
			pBufArg[nPos1++] = buf2[nPos2]; //important
			if (buf2[nPos2] == 1) {
				nPos2++;
				break;
			} else
				nPos2 = 0;
		}
	}

	if (bEOF) {
		fseek(fp, 0, SEEK_SET);
		//printf("  h264_GetOneFrameFromFile(..):reach the tail of file\n");

	} else
		fseek(fp, -4, SEEK_CUR);

	return (nPos1 - 4);
}

//@return	>=0 success
//			<0  fail
//				-1: non-h264 file
//
INT32 myGetOneFrame_video(FILE *fp, CHAR *out_pBuf, INT32 nBufMaxSize,
		CHAR *out_pchFrameType) {
	CHAR *bufTmp = (CHAR *) malloc(MAX_SIZE_BUF);
	INT32 nSize = 0, nTotalSize = 0;
	bool bIFrame = false;

	memset(bufTmp, 0, MAX_SIZE_BUF);
	do {
		nSize = h264_GetOneFrameFromFile(fp, bufTmp);
		if (nSize <= 0) {
			nTotalSize = nSize; //-1, error code
			break; //break----------------

		} else {
			if (out_pBuf)
				memcpy(out_pBuf + nTotalSize, bufTmp, nSize);
			nTotalSize += nSize;

			if ((bufTmp[4] & 0x07) == 0x07) {
				bIFrame = true;

				//read 2nd time
				nSize = h264_GetOneFrameFromFile(fp, bufTmp);
				if (nSize <= 0) {
					nTotalSize = nSize; //-1, error code
					break; //break----------------
				} else {
					if (out_pBuf)
						memcpy(out_pBuf + nTotalSize, bufTmp, nSize);
					nTotalSize += nSize;
				}

				//read 3rd time
				nSize = h264_GetOneFrameFromFile(fp, bufTmp);
				if (nSize <= 0) {
					nTotalSize = nSize; //-1, error code
					break; //break----------------
				} else {
					if (out_pBuf)
						memcpy(out_pBuf + nTotalSize, bufTmp, nSize);
					nTotalSize += nSize;
				}
			} else
				bIFrame = false;
		}

		if (out_pchFrameType != NULL) {
			if (bIFrame)
				*out_pchFrameType = VFRAME_FLAG_I;
			else
				*out_pchFrameType = VFRAME_FLAG_P;
		}
	} while (0);

	free(bufTmp);
	return nTotalSize;
}

//=={{audio: ADPCM codec==============================================================
INT32 g_nAudioPreSample = 0;
INT32 g_nAudioIndex = 0;

static int gs_index_adjust[8] = { -1, -1, -1, -1, 2, 4, 6, 8 };
static int gs_step_table[89] = { 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 21,
		23, 25, 28, 31, 34, 37, 41, 45, 50, 55, 60, 66, 73, 80, 88, 97, 107,
		118, 130, 143, 157, 173, 190, 209, 230, 253, 279, 307, 337, 371, 408,
		449, 494, 544, 598, 658, 724, 796, 876, 963, 1060, 1166, 1282, 1411,
		1552, 1707, 1878, 2066, 2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428,
		4871, 5358, 5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635,
		13899, 15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767 };

void Encode(unsigned char *pRaw, int nLenRaw, unsigned char *pBufEncoded) {
	short *pcm = (short *) pRaw;
	int cur_sample;
	int i;
	int delta;
	int sb;
	int code;
	nLenRaw >>= 1;

	for (i = 0; i < nLenRaw; i++) {
		cur_sample = pcm[i];
		delta = cur_sample - g_nAudioPreSample;
		if (delta < 0) {
			delta = -delta;
			sb = 8;
		} else
			sb = 0;

		code = 4 * delta / gs_step_table[g_nAudioIndex];
		if (code > 7)
			code = 7;

		delta = (gs_step_table[g_nAudioIndex] * code) / 4
				+ gs_step_table[g_nAudioIndex] / 8;
		if (sb)
			delta = -delta;

		g_nAudioPreSample += delta;
		if (g_nAudioPreSample > 32767)
			g_nAudioPreSample = 32767;
		else if (g_nAudioPreSample < -32768)
			g_nAudioPreSample = -32768;

		g_nAudioIndex += gs_index_adjust[code];
		if (g_nAudioIndex < 0)
			g_nAudioIndex = 0;
		else if (g_nAudioIndex > 88)
			g_nAudioIndex = 88;

		if (i & 0x01)
			pBufEncoded[i >> 1] |= code | sb;
		else
			pBufEncoded[i >> 1] = (code | sb) << 4;
	}
}

void Decode(char *pDataCompressed, int nLenData, char *pDecoded) {
	int i;
	int code;
	int sb;
	int delta;
	short *pcm = (short *) pDecoded;
	nLenData <<= 1;

	for (i = 0; i < nLenData; i++) {
		if (i & 0x01)
			code = pDataCompressed[i >> 1] & 0x0f;
		else
			code = pDataCompressed[i >> 1] >> 4;

		if ((code & 8) != 0)
			sb = 1;
		else
			sb = 0;
		code &= 7;

		delta = (gs_step_table[g_nAudioIndex] * code) / 4
				+ gs_step_table[g_nAudioIndex] / 8;
		if (sb)
			delta = -delta;

		g_nAudioPreSample += delta;
		if (g_nAudioPreSample > 32767)
			g_nAudioPreSample = 32767;
		else if (g_nAudioPreSample < -32768)
			g_nAudioPreSample = -32768;

		pcm[i] = g_nAudioPreSample;
		g_nAudioIndex += gs_index_adjust[code];
		if (g_nAudioIndex < 0)
			g_nAudioIndex = 0;
		if (g_nAudioIndex > 88)
			g_nAudioIndex = 88;
	}
}
//==}}audio: ADPCM codec==============================================================

#define ADPCM_ENCODE_BYTE_SIZE	160
#define ADPCM_DECODE_BYTE_SIZE	640

#define READSIZE_EVERY_TIME		1920

UCHAR g_out_pBufTmp[READSIZE_EVERY_TIME];

// get 1920 bytes every time
INT32 myGetAudioData(FILE *fp, CHAR *out_pBuf, INT32 nBufMaxSize,
		CHAR *out_pchFrameType) {
	INT32 nReadSize = 0, nSize = 0;

	nReadSize = fread(g_out_pBufTmp, 1, READSIZE_EVERY_TIME, fp);
	if (nReadSize < READSIZE_EVERY_TIME) {
		fseek(fp, 0, SEEK_SET);
		//printf("  myGetAudioData(..):reach the tail of file\n");

		memset(out_pBuf, 0, nBufMaxSize);
		nSize = READSIZE_EVERY_TIME / ADPCM_DECODE_BYTE_SIZE
				* ADPCM_ENCODE_BYTE_SIZE;
	} else {
		UCHAR bufSmall[ADPCM_ENCODE_BYTE_SIZE];
		nSize = 0;
		for (int i = 0; i < nReadSize / ADPCM_DECODE_BYTE_SIZE; i++) {
			Encode(g_out_pBufTmp + i * ADPCM_DECODE_BYTE_SIZE,
			ADPCM_DECODE_BYTE_SIZE, bufSmall);
			memcpy(out_pBuf + nSize, bufSmall, ADPCM_ENCODE_BYTE_SIZE);
			nSize += ADPCM_ENCODE_BYTE_SIZE;
		}
	}

	return nSize;
}

#ifdef WIN32DLL
DWORD WINAPI myThreadGetDataFromFile(void* arg)
#endif
#ifdef LINUX
void *myThreadGetDataFromFile(void *arg)
#endif
		{
	CHAR strVideoFile[300] = { 0 }, strAudioFile[300] = { 0 };
	CHAR sAppPath[256] = { 0 };
	int nLenPath = 0;
	getcwd(sAppPath, sizeof(sAppPath));
	nLenPath = strlen(sAppPath);

#ifdef WIN32DLL
	if(sAppPath[nLenPath-1]!='\\') strcat(sAppPath, "\\");
	strcpy(strVideoFile, sAppPath); strcat(strVideoFile, "AVData\\V.h264");
	strcpy(strAudioFile, sAppPath); strcat(strAudioFile, "AVData\\8K_16_1.pcm");
#endif //// #ifdef WIN32DLL
#ifdef LINUX
	if (sAppPath[nLenPath - 1] != '/')
		strcat(sAppPath, "/");
	strcpy(strVideoFile, sAppPath);
	strcat(strVideoFile, "AVData/V.h264");
	strcpy(strAudioFile, sAppPath);
	strcat(strAudioFile, "AVData/8K_16_1.pcm");
#endif //// #ifdef LINUX

	INT32 size = 0, nHeadLen = sizeof(st_AVStreamIOHead)
			+ sizeof(st_AVFrameHead);
	CHAR *pBuf = (CHAR *) malloc(MAX_SIZE_BUF);
	CHAR *pBufVideo = &pBuf[nHeadLen];
	CHAR *pBufAudio = pBufVideo;
	UINT32 TheCounter = 0;

	st_AVStreamIOHead *pstStreamIOHead = (st_AVStreamIOHead *) pBuf;
	st_AVFrameHead *pstFrameHead =
			(st_AVFrameHead *) &pBuf[sizeof(st_AVStreamIOHead)];
	pstFrameHead->nOnlineNum = 1;
	ULONG nCurTick = 0L;

	FILE *fpH264 = fopen(strVideoFile, "rb");
	FILE *fpAudio = fopen(strAudioFile, "rb");
	do {
		if (fpH264 == NULL || fpAudio == NULL) {
			printf("Failed to open audio or video file.");
			break;
		}

		while (gRunning) {
			nCurTick = myGetTickCount();
			if (TheCounter % 4 == 0) // for Audio 80 ms
					{
				size = myGetAudioData(fpAudio, pBufAudio,
				MAX_SIZE_BUF - nHeadLen, (CHAR *) &(pstFrameHead->flag));
				if (size > 0) {
					pstFrameHead->nCodecID = CODECID_A_ADPCM;
					pstFrameHead->nTimeStamp = nCurTick;
					pstFrameHead->nDataSize = size;

					pstStreamIOHead->nStreamIOHead = sizeof(st_AVFrameHead)
							+ size;
					pstStreamIOHead->uionStreamIOHead.nStreamIOType =
							SIO_TYPE_AUDIO;

					AudioFrameReady(pBuf, size + nHeadLen, pstFrameHead);
				}
			}
			if (TheCounter % 10 == 0) // Video 10 Hz
					{
				size = myGetOneFrame_video(fpH264, pBufVideo,
				MAX_SIZE_BUF - nHeadLen, (CHAR *) &(pstFrameHead->flag));
				if (size > 0) {
					pstFrameHead->nCodecID = CODECID_V_H264;
					pstFrameHead->nTimeStamp = nCurTick;
					pstFrameHead->nDataSize = size;

					pstStreamIOHead->nStreamIOHead = sizeof(st_AVFrameHead)
							+ size;
					pstStreamIOHead->uionStreamIOHead.nStreamIOType =
							SIO_TYPE_VIDEO;

					for (INT32 i = 0; i < MAX_USER; i++) {
						if (!gUser[i].bUsed || !gUser[i].bVideoRequested)
							continue;
						if (pstFrameHead->flag == 0)
							gUser[i].bVideoGo = 1;
					}

					VideoFrameReady(pBuf, size + nHeadLen, pstFrameHead);
				}
			}

			mSecSleep(TIME_SPAN_IDLE);
			TheCounter++;
		} //while-end
	} while (0);

	if (fpH264)
		fclose(fpH264);
	if (fpAudio)
		fclose(fpAudio);
	free(pBuf);

#ifdef WIN32DLL
	return 0L;
#endif
#ifdef LINUX
	pthread_exit(0);
#endif
}

#endif //// #ifdef AVSTREAM_TEST

INT32 main(INT32 argc, CHAR **argv) {
#ifdef AVSTREAM_TEST
	if (argc != 2) {
		printf("Usage: StreamDemo_Device DeviceID\n");
		printf("Example: StreamDemo_Device XXX-000000-ZZZZZ\n");
		return 0;
	}
	memset(gUser, 0, sizeof(st_User) * MAX_USER);

	// To Do ... Create a thread to read AV data from file and call FrameReady()
	STARX_threadid threadIDrecv;
	memset(&threadIDrecv, 0, sizeof(threadIDrecv));

#ifdef WIN32DLL
	void *hThread=NULL;
	hThread = CreateThread(NULL, 0, myThreadGetDataFromFile, (LPVOID)NULL, 0, &threadIDrecv);
	if(NULL!=hThread) CloseHandle(hThread);
	else
#endif
#ifdef LINUX
	if (pthread_create(&threadIDrecv, NULL, &myThreadGetDataFromFile,
			(void *) NULL) == 0) {
		pthread_detach(threadIDrecv);
	} else
#endif
	{
		gRunning = 0;
		return 0;
	}

#endif //// #ifdef AVSTREAM_TEST

	UINT32 APIVersion = STARX_GetAPIVersion();
	printf("STARX_API Version: %d.%d.%d.%d\n", (APIVersion & 0xFF000000) >> 24,
			(APIVersion & 0x00FF0000) >> 16, (APIVersion & 0x0000FF00) >> 8,
			(APIVersion & 0x000000FF) >> 0);

	INT32 ret;
	ret =
			STARX_Initialize(
					(CHAR*) "EFGBFFBJKFJOGCJNFHHCFHEMGENHHBMHHLFGBKDFAMJLLDKHDHACDEPBGCLAIALDADMPKDDIODMEBOCNJLNDJJ");

	st_STARX_NetInfo NetInfo;
	ret = STARX_NetworkDetect(&NetInfo, 0);

	printf("-------------- NetInfo: -------------------\n");
	printf("Internet Reachable     : %s\n",
			(NetInfo.bFlagInternet == 1) ? "YES" : "NO");
	printf("P2P Server IP resolved : %s\n",
			(NetInfo.bFlagHostResolved == 1) ? "YES" : "NO");
	printf("P2P Server Hello Ack   : %s\n",
			(NetInfo.bFlagServerHello == 1) ? "YES" : "NO");
	printf("Local NAT Type         :");
	STARX_Share_Bandwidth(1);
	switch (NetInfo.NAT_Type) {
	case 0:
		printf(" Unknow\n");
		break;
	case 1:
		printf(" IP-Restricted Cone\n");
		break;
	case 2:
		printf(" Port-Restricted Cone\n");
		break;
	case 3:
		printf(" Symmetric\n");
		break;
	}
	printf("My Wan IP : %s\n", NetInfo.MyWanIP);
	printf("My Lan IP : %s\n", NetInfo.MyLanIP);

#ifdef AVSTREAM_TEST
	int pp = 0;
	while (1) {
		INT32 SessionHandle = 0;
		pp++;
		SessionHandle = STARX_Listen(argv[1], 600, 10000 + pp, 1);
		if (SessionHandle >= 0) {

			st_STARX_Session Sinfo;
			if (STARX_Check(SessionHandle, &Sinfo) == ERROR_STARX_SUCCESSFUL) {
				printf("\n-------Session(%d) Ready %s------------------\n",
						SessionHandle, (Sinfo.bMode == 0) ? "P2P" : "RLY");
				printf("Socket FD: %d\n", Sinfo.Skt);
				printf("Remote Address : %s:%d\n",
						inet_ntoa(Sinfo.RemoteAddr.sin_addr),
						ntohs(Sinfo.RemoteAddr.sin_port));
			}

			INT32 i;
			for (i = 0; i < MAX_USER; i++) {
				if (!gUser[i].bUsed)
					break;
			}
			if (i == MAX_USER)
				continue;

			printf("i=%d connected\n", i);
			gUser[i].SessionHandle = SessionHandle;
			gUser[i].bUsed = 1;

#ifdef WIN32DLL
			void *hThread=NULL;
			hThread = CreateThread(NULL, 0, myThreadRecvIOCtrl, (LPVOID)i, 0, &threadIDrecv);
			if(NULL!=hThread) CloseHandle(hThread);
			else
#endif
#ifdef LINUX
			if (pthread_create(&threadIDrecv, NULL, &myThreadRecvIOCtrl,
					(void *) i) == 0) {
				pthread_detach(threadIDrecv);
			} else
#endif
			{
				gRunning = 0;
				return 0;
			}
		} else if (SessionHandle == ERROR_STARX_INVALID_ID) {
			printf("Invalid DID \n");
			break;
		}
	}
#endif
	ret = STARX_DeInitialize();
	printf("....Job Done!! press any key to exit\n");
	getchar();
	return 0;
}
