DEV_DID=STAR-000105-HPTLB
CFLAGS= -g -O2 -Wall -DLINUX

CXX=~/buildroot-gcc342/bin/mipsel-linux-g++
ARCH=mips

CXX=g++
ARCH=x86

all: 
	$(CXX) $(CFLAGS) -Iinclude src/StreamDemo_Device.cpp lib/$(ARCH)/libPPPP_API.so -o StreamDemo_Device -s -lpthread
	$(CXX) $(CFLAGS) -Iinclude src/TransferFileDemo_Device.cpp lib/$(ARCH)/libPPPP_API.so -o TransferFileDemo_Device -s
	$(CXX) $(CFLAGS) -Iinclude src/TransferFileDemo_Client.cpp lib/$(ARCH)/libPPPP_API.so -o TransferFileDemo_Client -s
test_StreamDemo_Device: all
	@echo 请安装当前目录下的Android APK到手机上进行测试
	./StreamDemo_Device $(DEV_DID)
	
test_TransferFileDemo_Device: all
	./TransferFileDemo_Device $(DEV_DID) ./README
	
test_TransferFileDemo_Client: all
	./TransferFileDemo_Client $(DEV_DID) /tmp/data

clean:
	rm -rf *.o *~ *.bak StreamDemo_Device TransferFileDemo_Client TransferFileDemo_Device

